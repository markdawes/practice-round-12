package com.rave.practiceround12.model

import com.rave.practiceround12.model.local.Book
import com.rave.practiceround12.model.remote.APIService
import javax.inject.Inject

/**
 * Math repo.
 *
 * @property service
 * @constructor Create empty Math repo
 */
class MathRepo @Inject constructor(private val service: APIService) {

    /**
     * Get math books.
     *
     * @return
     */
    suspend fun getMathBooks(): List<Book> {
        val bookDTOs = service.getMathBooks().works
        return bookDTOs!!.map {
            Book(
                title = it.title!!
            )
        }
    }
}
