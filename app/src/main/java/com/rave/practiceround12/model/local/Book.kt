package com.rave.practiceround12.model.local

/**
 * Book.
 *
 * @property title
 * @constructor Create empty Book
 */
data class Book(
    val title: String
)
