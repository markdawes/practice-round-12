package com.rave.practiceround12

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Main class for dagger/hilt.
 *
 * @constructor Create empty Math application
 */
@HiltAndroidApp
class MathApplication : Application()
